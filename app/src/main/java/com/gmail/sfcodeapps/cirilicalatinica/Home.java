package com.gmail.sfcodeapps.cirilicalatinica;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;

public class Home extends AppCompatActivity {

    int TXT_CODE = 222;
    EditText etUp;
    EditText etDown;
    Button btnPaste;
    Button btnCopy;
    Button btnImport;
    Button btnExport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
        }

        etUp = findViewById(R.id.etUp);
        etDown = findViewById(R.id.etDown);

        FloatingActionButton fabTranslate = findViewById(R.id.fabTranslate);
        fabTranslate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(Home.this);
                String convertedText = cyrllicToLatin(etUp.getText().toString());
                if(convertedText.equals(etUp.getText().toString())){
                    etDown.setText(latinToCyrllic(etUp.getText().toString()));
                }
                else {
                    etDown.setText(convertedText);
                }
            }
        });

        btnPaste = findViewById(R.id.btnPaste);
        btnPaste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etUp.setText("");
                etDown.setText("");
                pasteFromClipboard();
            }
        });

        btnCopy = findViewById(R.id.btnCopy);
        btnCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyToCliboard();
                showSweetDialog(getResources().getString(R.string.text_copied));
            }
        });

        btnImport = findViewById(R.id.btnImport);
        btnImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTextFile();
            }
        });

        btnExport = findViewById(R.id.btnExport);
        btnExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createTextFile(etDown.getText().toString());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TXT_CODE)
        {
            if (resultCode == RESULT_OK)
            {
                Uri uri = data.getData();
                String fileContent = readTextFile(uri);
                etUp.setText(fileContent);
            }
        }
    }

    private void showSweetDialog(String title, String content){
        new SweetAlertDialog(Home.this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(title)
                .setContentText(content)
                .show();
    }

    private void showSweetDialog(String title){
        new SweetAlertDialog(Home.this)
                .setTitleText(title)
                .show();
    }

    private void showSweetError(String title){
        new SweetAlertDialog(Home.this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .show();
    }

    private String createFileName(String content){
        SimpleDateFormat formatter = new SimpleDateFormat("HH_mm_dd_MM_yyyy", Locale.US);
        Date now = new Date();

        if(content.length() < 20){
            return content + "_" + formatter.format(now) + ".txt";
        }
        else{
            return content.substring(0, 19) + "_" + formatter.format(now) + ".txt";
        }
    }

    private String readTextFile(Uri uri) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(getContentResolver().openInputStream(uri)));

            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

        public void copyToCliboard(){
            ClipboardManager clipboard = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("string", etDown.getText().toString());
            clipboard.setPrimaryClip(clip);
        }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //granted
                } else {
                    //not granted
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void selectTextFile(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/plain");
        startActivityForResult(intent, TXT_CODE);
    }

    private void createTextFile(String data) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Cirilica-Latinica");
            if (!root.exists()) {
                Log.d("TextLOG", "creating folder");
                boolean success = root.mkdirs();
                Log.d("TextLOG", "folder created = " + success);
            }
            File gpxfile = new File(root, createFileName(data));
            boolean success = gpxfile.createNewFile();
            Log.d("TextLOG", "file created= " + success);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(data);
            writer.flush();
            Log.d("TextLOG", "adding text= " + data + " to " + gpxfile.getAbsolutePath());
            writer.close();
            showSweetDialog(getResources().getString(R.string.file_created), "" + gpxfile.getAbsolutePath());
          //  Toast.makeText(context, "Datoteka je kreirana!", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Log.d("TextLOG", "error= " + e);
            e.printStackTrace();
            showSweetError(getResources().getString(R.string.file_not_created));
        }
    }

        public void pasteFromClipboard(){
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            String pasteData = "";

            if (!(clipboard.hasPrimaryClip())) {
            } else if (!(clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN))) {
            } else {
                ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                Log.d("ClipLOG", "item" + item);
                pasteData = item.getText().toString();
                Log.d("ClipLOG", "pasteData" + pasteData);
                etUp.setText(pasteData);
            }

        }

    public static String cyrllicToLatin(String message){
        boolean appended = false;
        char[] abcCyr =   {'0','1','2','3','4','5','6','7','8','9',' ','-',',','а','б','в','г','д','ђ','е','ж','з','и','ј','к','л','љ','м','н','њ','о','п','р','с','т','ћ','у','ф','х','ц','ч','џ','ш','А','Б','В','Г','Д','Ђ','Е','Ж','З','И','Ј','К','Л','Љ','М','Н','Њ','О','П','Р','С','Т','Ћ','У','Ф','Х', 'Ц', 'Ч','Џ','Ш','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        String[] abcLat = {"0","1","2","3","4","5","6","7","8","9"," ","-",",","a","b","v","g","d","đ","e","ž","z","i","j","k","l","lj","m","n","nj","o","p","r","s","t","ć","u","f","h","c","č","dž","š","A","B","V","G","D","Đ","E","Ž","Z","I","J","K","L","Lj","M","N","Nj","O","P","R","S","T","Ć","U","F","H","C","Č","Dž","Š","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            for (int x = 0; x < abcCyr.length; x++ ) {
                if (message.charAt(i) == abcCyr[x]) {
                    builder.append(abcLat[x]);
                    Log.d("CharLOG", "converted " + abcCyr[x] + " to " + abcLat[x]);
                    appended = true;
                }
            }
            Log.d("CharLOG", "appended= " + appended);
            if (!appended) {
                builder.append(message.charAt(i));
                Log.d("CharLOG", "added= " + message.charAt(i));
            }
            appended = false;
        }
        return builder.toString();
    }

    public static String latinToCyrllic(String message){
        boolean appended = false;
        char[] abcCyr =   {'0','1','2','3','4','5','6','7','8','9',' ','-',',','а','б','в','г','д','ђ','е','ж','з','и','ј','к','л','љ','м','н','њ','о','п','р','с','т','ћ','у','ф','х','ц','ч','џ','ш','А','Б','В','Г','Д','Ђ','Е','Ж','З','И','Ј','К','Л','Љ','М','Н','Њ','О','П','Р','С','Т','Ћ','У','Ф','Х', 'Ц', 'Ч','Џ','Ш'};
        String[] abcLat = {"0","1","2","3","4","5","6","7","8","9"," ","-",",","a","b","v","g","d","đ","e","ž","z","i","j","k","l","lj","m","n","nj","o","p","r","s","t","ć","u","f","h","c","č","dž","š","A","B","V","G","D","Đ","E","Ž","Z","I","J","K","L","Lj","M","N","Nj","O","P","R","S","T","Ć","U","F","H","C","Č","Dž","Š"};
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < message.length(); i++) {
            for (int x = 0; x < abcLat.length; x++ ) {
                String latChar = Character.toString(message.charAt(i));
                if (latChar.equals(abcLat[x])) {
                    builder.append(abcCyr[x]);
                    appended = true;
                }
            }
            if (!appended) builder.append(message.charAt(i));
            appended = false;
        }
        return builder.toString();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
